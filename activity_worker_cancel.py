import boto3
import sys
import argparse
import requests
import json
import logging
import os

logger = logging.getLogger()
streamHandler = logging.StreamHandler(sys.stdout)
streamHandler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)


def run_worker(token, inputs, sf_client, s3_client, ec2_client, s3_bucket_name):
    try:
        logger.info('Clean up worker started')
        print('Clean up Worker started')
        print('Deleting File on EFS')
        filePath = "/mnt/efs/" + inputs['userId'] +"/"+ inputs['mediaId'] + "/*"
        command = "rm -f "+ filePath
        os.system(command)
        print('File deleted from EFS')
        print('Deleting S3 media assets')
       # s3_bucket = s3_resource.Bucket('graymeta-iris-media-storage')
        s3_bucket = s3_resource.Bucket(s3_bucket_name)
        key_prefix = inputs['userId'] +"/"+ inputs['mediaId']
        for obj in s3_bucket.objects.filter(Prefix=key_prefix):
            s3_bucket.delete_objects(Delete ={'Objects' :[{'Key': obj.key}]})
        print('Media assets deleted successfully')
        print('Terminating ec2 instances')
        sf_response = sf_client.send_task_success(
               taskToken=token,
               output=json.dumps({'Result' : 'Clean Up task completed successfully'})
        )
        print(sf_response)
        logger.info(sf_response)
        waiter = ec2_client.get_waiter('instance_terminated')
        ec2_client.terminate_instances(
             InstanceIds=list(inputs['instances'].values())
        )
        waiter.wait(InstanceIds=list(inputs['instances'].values()))
        print('EC2 instances terminated successfully')
        print("All done")
      #  logger.info(sf_response)
      #  logger.info('activity task completed successfully')
    except Exception as e:
        sf_client.send_task_failure(
            taskToken=token,
            error= "Error occurred while cleaning up the resources",
            cause=e)


if __name__ == "__main__":

    try:
        parser = argparse.ArgumentParser()
        parser.add_argument('--activity_arn', help="Specify an Activity ARN", type=str)
        parser.add_argument('--media_bucket_name', help="Specify media_bucket_name", type=str)
        parser.add_argument('--region', help="Specify region", type=str)
        args=parser.parse_args()
        region = args.region
        if args.activity_arn == None:
            print('Usage: python activity_worker_cancel.py --activity_arn value')
            sys.exit(1)

        boto3.setup_default_session(region_name=region)
        print('Step functions started')
        sf_client = boto3.client('stepfunctions')
        s3_resource = boto3.resource('s3')
        ec2_client = boto3.client('ec2')
        print("Activity Arn:", args.activity_arn)
        if args.activity_arn is not None:
            activity_arn = args.activity_arn
            s3_bucket_name = args.media_bucket_name
            response = sf_client.get_activity_task(
                                activityArn=activity_arn,
                                workerName='cleanup-worker'
                        )
            logger.info("Got Activity Task")
            print('Got Activity Task')
            inputs = json.loads(response['input'])
            token = response['taskToken']
            run_worker(token, inputs, sf_client, s3_resource, ec2_client, s3_bucket_name)
            exit(1)

        else:
            logger.info('Please specify Activity ARN and Ip')
            exit(1)
    except Exception as e:
        logger.error("Error occurred while processing the activity task state")
        print("Error : ", e)
        exit(1)
