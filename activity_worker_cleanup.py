import boto3
import sys
import argparse
import requests
import json
import logging
import os

logger = logging.getLogger()
streamHandler = logging.StreamHandler(sys.stdout)
streamHandler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)


def run_worker(token, inputs, sf_client, ec2_client, queue):
    try:
        logger.info('Clean up worker started')
        print('Clean up Worker started')
        print('Deleting File %s from EFS' % inputs['fileId'])
        command = "rm -f "+ inputs['fileId']
        os.system(command)
        print('File %s deleted from EFS' % inputs['fileId'])
        print('Terminating ec2 instances')
        ec2_client.terminate_instances(
             InstanceIds=list(inputs['instances'].values())
        )
        print('EC2 instances terminated successfully')
        sf_response = sf_client.send_task_success(
               taskToken=token,
               output=json.dumps({'Result' : 'Clean Up task completed successfully'})
        )
        sqs_send_message(queue, inputs, 'COMPLETED')
        logger.info(sf_response)
        logger.info('activity task completed successfully')
    except Exception as e:
        sf_client.send_task_failure(
            taskToken=token,
            error= "Error occurred while cleaning up the resources",
            cause=e)

def sqs_send_message(queue, inputs, status):
    messagebody = {"mediaId": inputs["mediaId"], "status": status}
    bodystr=json.dumps(messagebody)
    print(bodystr)
    response = queue.send_message(
        MessageGroupId = 'media',
        MessageBody = bodystr
    )

if __name__ == "__main__":

    try:
        parser = argparse.ArgumentParser()
        parser.add_argument('--activity_arn', help="Specify an Activity ARN", type=str)
        parser.add_argument('--sqs_queue_name', help="Specify sqs_queue_name", type=str)
        parser.add_argument('--region', help="Specify region", type=str)
        args=parser.parse_args()
        region = args.region
        boto3.setup_default_session(region_name=region)
        if args.activity_arn == None:
            print('Usage: python activity_worker_cleanup.py --activity_arn value')
            sys.exit(1)

        print('Step functions started')
        sf_client = boto3.client('stepfunctions')
        ec2_client = boto3.client('ec2')
        sqs_resource = boto3.resource('sqs')
        #bufferName = 'irisCloud.fifo'
        bufferName = args.sqs_queue_name
        queue = sqs_resource.get_queue_by_name(QueueName=bufferName)
        print("Activity Arn:", args.activity_arn)
        if args.activity_arn is not None:
            activity_arn = args.activity_arn
            response = sf_client.get_activity_task(
                                activityArn=activity_arn,
                                workerName='cleanup-worker'
                        )
            logger.info("Got Activity Task")
            print('Got Activity Task')
            inputs = json.loads(response['input'])
            token = response['taskToken']
            run_worker(token, inputs, sf_client, ec2_client, queue)

        else:
            logger.info('Please specify Activity ARN and Ip')
            exit(1)
    except Exception as e:
        logger.error("Error occurred while processing the activity task state")
        print("Error : ", e)
        exit(1)
