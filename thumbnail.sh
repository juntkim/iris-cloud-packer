filename="$1"
outfilename="$3"
filepath="$6"
echo $outfilename
echo $filepath
total_frames=$(~/ffmpeg -i "$filename" -c copy -f null dev/null 2>&1 | grep 'frame=' | tr "=" "\n" | tail -7 | head -1 | tr -d 'a-z' | tr -d " ")
numframes=$2	
echo $total_frames
if [ "$total_frames" == "" ]; then
	echo "Error[101] Check getting total_frames command", $total_frames
	exit 101
fi
echo $numframes
rate=$(echo "scale=0; $total_frames/($numframes-1)" | bc)
echo $rate
$(~/ffmpeg -y -i "$filename" -s 1500x40 -vf "select=not(mod(n\,$rate)),tile=$2x1" "$filepath"/"$outfilename.png")
if [ "$?" -ne 0 ]; then
	echo "Error[102] Check getting a thumbnail command"
	exit 102
fi
#aws s3 cp "$filepath"/"$outfilename.png" s3://graymeta-iris-media-storage/$4/$5/thumbnail/"$outfilename.png"
#if [ "$?" -ne 0 ]; then
#	echo "Error[103] Check uploading PNG files command"
#	exit 103
#fi