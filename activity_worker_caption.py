import boto3
import sys
import argparse
import requests
import json
import logging
import os

logger = logging.getLogger()
streamHandler = logging.StreamHandler(sys.stdout)
streamHandler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)

#LOGS_BUCKET_NAME = 'graymeta-iris-logs'
LOGS_PREFIX = None

def run_worker(token, inputs, sf_client, s3_bucket, queue, logs_bucket_name):
    try:
        logger.info('worker started')
        logger.info('Extracting subtitles info from video streams')
        command = "mkdir -p ~/temp && ffprobe -i \""+ inputs['fileId'] +"\" -v quiet -print_format json -show_error -show_streams -show_format -v error > ~/temp/info.json"
        logger.info('Extracted subtitles info from the videos streams')
        logger.info('Creating subtitle files')
        os.system(command)
        base_name = os.path.basename(inputs['fileId'])
        file_name = os.path.splitext(base_name)[0]
        caption_data = []
        with open('/home/ec2-user/temp/info.json') as json_file:
            data = json.load(json_file)
            language_counter = {}
            for stream in data['streams']:
                if stream["codec_type"] == 'subtitle':
                    counter_string = None
                    language = 'unknown'
                    if 'tags' in stream and 'language' in stream['tags']:
                        language = stream['tags']['language']
                    if language in language_counter:
                        language_counter[language] += language_counter[language]
                        counter_string = "{0:0{1}d}".format(language_counter[language], 3)
                    else:
                        language_counter[language] = 1
                        counter_string = "{0:0{1}d}".format(language_counter[language], 3)
                    command = "ffmpeg -v quiet -i \"" + inputs['fileId'] + "\" -map 0:"+ str(stream['index'])+" ~/temp/"+file_name+"_"+language+"_"+counter_string+".vtt"
                    print(command)
                    subtitle = {}
                    subtitle['fileName'] = file_name+"_"+language+"_"+counter_string+".vtt"
                    subtitle['language'] = language
                    subtitle['mediaId'] = inputs['mediaId']
                    subtitle['status'] = 'INIT'
                    caption_data.append(subtitle)
                    os.system(command)

        captions = caption_data 

        if len(captions) > 0:
            sqs_send_message(queue, captions, "SUCCESS" )

        if len(captions) > 0:
            for file_data in captions:
                print('inside loop')
                file_data['status'] = 'IN_PROGRESS'
                print('inside loop')
            sqs_send_message(queue, captions, "SUCCESS")

        logger.info('Created subtitles with desired format')
        logger.info('Uploading subtitles to s3 bucket')
        os.system("rm -f /home/ec2-user/temp/info.json")

        if len(captions) > 0:
            for file_data in captions:
                file_data['status'] = 'CREATED'
            sqs_send_message(queue, captions, "SUCCESS")

        print('Uploading subtitiles to s3 bucket')
        s3_path = inputs['userId']+"/"+inputs['mediaId']+"/captions/"
        logger.info(s3_path)
        for r, d, f in os.walk("/home/ec2-user/temp/"):
            for file in f:
                source_path = "/home/ec2-user/temp/"+file
                target_path = s3_path + file
                print(source_path, target_path)
                s3_bucket.upload_file(source_path, target_path)

        logger.info('Uploaded subtitles files to s3 bucket')
        os.system("rm -rf /home/ec2-user/temp")

        if len(captions) > 0:
            for file_data in captions:
                file_data['status'] = 'UPLOADED'
            sqs_send_message(queue, captions, "SUCCESS")

        sf_response = sf_client.send_task_success(
            taskToken=token,
            output=json.dumps(inputs)
        )
        logger.info(sf_response)
        logger.info('Subtitle Stream Extractor task completed successfully')
            
    #    else:
    #        messages = {'301': 'Server Error: notify support (301)'}
    #        captions["error_msg"] = messages.get('401') 
    #        sqs_send_message(queue, captions, "FAILED")
    #        sf_client.send_task_failure(
    #        taskToken=token,
    #        error= "Error occurred while processing subtitles extraction",
    #        cause=captions["error_msg"])
          #  sf_response = sf_client.send_task_success(
          #         taskToken=token,
          #         output=json.dumps(inputs)
          #  )
     #   logger.info(sf_response)
      #  logger.info('Subtitle Stream Extractor task completed successfully')
    except Exception as e:
        sf_client.send_task_failure(
            taskToken=token,
            error= "Error occurred while processing subtitles extraction",
            cause=e)
        captions = inputs
        captions["error_msg"] = e
        sqs_send_message(queue, captions, "FAILED")
        s3_resource.object(logs_bucket_name, LOGS_PREFIX+'logs.txt').put(Body = e)

def sqs_send_message(queue,captions, status):
    if status =='SUCCESS':
        messagebody = {"captions": captions}
        bodystr=json.dumps(messagebody)
        print(bodystr)
        response = queue.send_message(
            MessageGroupId = 'caption',
            MessageBody = bodystr
        )
    else:
        messagebody = {"mediaId": captions["mediaId"], 
                        "status": status,
                        "errorCause": captions["error_msg"]}
        bodystr=json.dumps(messagebody)
        print(bodystr)
        response = queue.send_message(
            MessageGroupId = 'media',
            MessageBody = bodystr
        )

if __name__ == "__main__":

    try:
        parser = argparse.ArgumentParser()
        parser.add_argument('--activity_arn', help="Specify an Activity ARN", type=str)
        parser.add_argument('--sqs_queue_name', help="Specify sqs_queue_name", type=str)
        parser.add_argument('--media_bucket_name', help="Specify media_bucket_name", type=str)
        parser.add_argument('--logs_bucket_name', help="Specify logs_bucket_name", type=str)
        parser.add_argument('--region', help="Specify region", type=str)
        args=parser.parse_args()
        region = args.region
        sqs_queue_name = args.sqs_queue_name
        s3_bucket_name = args.media_bucket_name
        logs_bucket_name = args.logs_bucket_name
        
        boto3.setup_default_session(region_name=region)
        s3_resource = boto3.resource('s3')
        print('Main function started')
        s3_bucket = s3_resource.Bucket(s3_bucket_name)
        #s3_bucket = s3_resource.Bucket('graymeta-iris-media-storage')

        sf_client = boto3.client('stepfunctions')
        sqs_resource = boto3.resource('sqs')
    #    bufferName = 'irisCloud.fifo'
        bufferName = sqs_queue_name
        queue = sqs_resource.get_queue_by_name(QueueName=bufferName)
        logger.info('Main function started')
        if args.activity_arn:   
            activity_arn = args.activity_arn
            response = sf_client.get_activity_task(
                                activityArn=activity_arn,
                                workerName='captions-extractor-worker'
                        )
            logger.info("Got Activity Task")
            inputs = json.loads(response['input'])
            token = response['taskToken']
            LOGS_PREFIX = inputs['userId'] + '/' + inputs['mediaId'] +'/captions/'
            run_worker(token, inputs, sf_client, s3_bucket, queue, logs_bucket_name)

        else:
            logger.info('Please specify Activity ARN')
            exit(1)
    except Exception as e:
        logger.error("Error occurred while processing the activity task state")
        print(e)
        captions = inputs
        captions['error_msg'] = e
        sqs_send_message(queue, captions, "FAILED")
        s3_resource.object(logs_bucket_name, LOGS_PREFIX+'logs.txt').put(Body = e)
        exit(1)
