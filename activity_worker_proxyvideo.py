import boto3
import sys
import argparse
import requests
import json
import logging
import os
import subprocess

logger = logging.getLogger()
streamHandler = logging.StreamHandler(sys.stdout)
streamHandler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)

#LOGS_BUCKET_NAME = 'graymeta-iris-logs'
LOGS_PREFIX = None

def run_worker(token, inputs, sf_client, s3_resource, queue, s3_bucket_name, logs_bucket_name):
    try:
        #s3_bucket = s3_resource.Bucket('graymeta-iris-media-storage')
        s3_bucket = s3_resource.Bucket(s3_bucket_name)
        logger.info('worker started')
        logger.info('Creating Videos with desired properites')
        video_files = []
        video_file = {}
        video_file['status'] = 'INIT'
        video_file['mediaId'] = inputs['mediaId']
        video_file['width'] ='640'
        video_file['height'] = '480'
        video_file['fileExtension'] = 'MP4'
        video_file['fileName'] = '640x480.mp4'
        video_file["progressiveResolution"]=  "480p"
        video_file["size"] = "118775157"
        video_files.append(video_file)
        sqs_send_message(queue, video_files, 'SUCCESS')
        video_files[0]['status'] = 'IN_PROGRESS'
        sqs_send_message(queue, video_files, 'SUCCESS')
        retVal = subprocess.call("ffmpeg  -i \""+ inputs['fileId'] +"\" -vf scale=640:480 -vcodec libx264 -preset fast -profile:v main -acodec aac ~/640x480.mp4 -hide_banner", shell=True)
        #command = "mkdir -p ~/temp && ffmpeg  -i \""+ inputs['fileId'] +"\" -vf scale=640:480 -vcodec libx264 -preset fast -profile:v main -acodec aac ~/temp/640x480.mp4 -hide_banner"
       # os.system(command)
       
        if retVal == 0:
            print('Created videos with desired dimensions')
            video_files[0]['status'] = 'CREATED'
            sqs_send_message(queue, video_files, 'SUCCESS')
            logger.info('Created videos with desired dimensions')
            logger.info('Uploading videos to s3 bucket')
            s3_path = inputs['userId']+"/"+inputs['mediaId']+"/proxy_videos"
            logger.info(s3_path)
            s3_bucket.upload_file("/home/ec2-user/640x480.mp4", s3_path+"/640x480.mp4")
            logger.info('Uploaded video files to s3 bucket')
            video_files[0]['status'] = 'UPLOADED'
            sqs_send_message(queue, video_files, 'SUCCESS')
            os.system("rm -rf ~/temp")
            sf_response = sf_client.send_task_success(
                       taskToken=token,
                       output=json.dumps(inputs)
            )
            logger.info(sf_response)
            logger.info('Video converter activity task completed successfully')
        else:
        
            messages = {'201': 'Server Error: notify support (201)'}
			#names.append(messages.get('201'))
            video_files[0]["error_msg"] = messages.get('201')
            sqs_send_message(queue, video_files, 'FAILED')
            sf_client.send_task_failure(
                taskToken=token,
                error= "Error occurred while processing metadata extraction",
                cause=video_files[0]["error_msg"])
            
         #   sf_response = sf_client.send_task_success(
         #       taskToken=token,
         #       output=json.dumps(inputs)
         #   )
    except Exception as e:
        sf_client.send_task_failure(
            taskToken=token,
            error= "Error occurred while processing metadata extraction",
            cause=e)
        video_files[0]['error_msg'] = e
        sqs_send_message(queue, inputs, 'FAILED')
        s3_resource.object(logs_bucket_name, LOGS_PREFIX+'logs.txt').put(Body = e)
        #s3_resource.object(LOGS_BUCKET_NAME, LOGS_PREFIX+'logs.txt').put(Body = e)


def sqs_send_message(queue, video_files, status):

    if status =='SUCCESS':
        messagebody = {"proxyVideos": video_files}
        bodystr=json.dumps(messagebody)
        print(bodystr)
        response = queue.send_message(
            MessageGroupId = 'proxyVideo',
            MessageBody = bodystr
        )
    else:
        messagebody = {"mediaId": video_files[0]["mediaId"], 
                        "status": status,
                        "errorCause": video_files[0]["error_msg"]}
        bodystr=json.dumps(messagebody)
        print(bodystr)
        response = queue.send_message(
            MessageGroupId = 'media',
            MessageBody = bodystr
        )
    

if __name__ == "__main__":

    try:
        inputs = {}
        LOGS_PREFIX =""
        parser = argparse.ArgumentParser()
        parser.add_argument('--activity_arn', help="Specify an Activity ARN", type=str)
        parser.add_argument('--logs_bucket_name', help="Specify logs_bucket_name", type=str)
        parser.add_argument('--sqs_queue_name', help="Specify sqs_queue_name", type=str)
        parser.add_argument('--media_bucket_name', help="Specify media_bucket_name", type=str)
        parser.add_argument('--region', help="Specify region", type=str)
        args=parser.parse_args()
        region = args.region
        boto3.setup_default_session(region_name=region)
        s3_resource = boto3.resource('s3')
        print('Main function started')
        sf_client = boto3.client('stepfunctions')
        sqs_resource = boto3.resource('sqs')
        bufferName=args.sqs_queue_name
        media_bucket_name = args.media_bucket_name
        logs_bucket_name = args.logs_bucket_name
    #    bufferName = 'irisCloud.fifo'
        queue = sqs_resource.get_queue_by_name(QueueName=bufferName)

        logger.info('Main function started')
        if args.activity_arn:
            activity_arn = args.activity_arn
            response = sf_client.get_activity_task(
                                activityArn=activity_arn,
                                workerName='proxy-videos-worker'
                        )
            logger.info("Got Activity Task")
            inputs = json.loads(response['input'])
            LOGS_PREFIX = inputs['userId'] + '/' + inputs['mediaId'] +'/proxyVideo/'
            token = response['taskToken']
            run_worker(token, inputs, sf_client, s3_resource, queue, media_bucket_name, logs_bucket_name)

        else:
            logger.info('Please specify Activity ARN')
            exit(1)
    except Exception as e:
        logger.error("Error occurred while processing the activity task state")
        print(e)
        inputs['error_msg'] = e
        sqs_send_message(queue, inputs, "FAILED")
        s3_resource.object(logs_bucket_name, LOGS_PREFIX+'logs.txt').put(Body = e)
        #s3_resource.object(LOGS_BUCKET_NAME, LOGS_PREFIX+'logs.txt').put(Body = e)
        exit(1)

