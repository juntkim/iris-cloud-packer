import boto3
import sys
import argparse
import requests
import json

LOGS_BUCKET_NAME = 'graymeta-iris-logs'
LOGS_PREFIX = None

def run_worker(token, inputs, rest_api_url, sf_client, queue, s3_resource, logs_bucket_name):

    print('worker started')
    inputs['status'] = 'IN_PROGRESS'
    sqs_send_message(queue, inputs, 'SUCCESS')
    print("inputs "+str(inputs))
    response = requests.post(rest_api_url, json=inputs)
    print("response "+str(response))
    status_code = response.status_code
    print("status_code "+str(status_code))
    response_data = json.loads(response.content)
    # data to be passed to next step
    output = {}
    output["instances"] = inputs["instances"]
    output["fileId"] = response_data['id']
    output["fileName"] = inputs["fileName"]
    output["mediaId"] = inputs["mediaId"]
    output["userId"] = inputs["userId"]
    print("status code = ",status_code)
    if (status_code == 201):
        sf_response = sf_client.send_task_success(
            taskToken=token,
            output=json.dumps(output))
        print(sf_response)
        print('Copy task completed successfully')
        return
    else:
        print('Error occurred')
      #  s3_resource.object(LOGS_BUCKET_NAME, LOGS_PREFIX+'logs.txt').put(Body = e)
        s3_resource.object(logs_bucket_name, LOGS_PREFIX+'logs.txt').put(Body = e)
        error = "Status code: " + str(status_code)
        sf_client.send_task_failure(
            taskToken=token,
            error= error,
            cause=response.content)
        inputs['error_msg'] = error
        sqs_send_message(queue, inputs, "FAILED")

def sqs_send_message(queue, inputs, status):
    if status =='SUCCESS':
        messagebody = {"mediaId": inputs["mediaId"], 
                        "status": inputs['status']}
        bodystr=json.dumps(messagebody)
        print(bodystr)
        response = queue.send_message(
            MessageGroupId = 'media',
            MessageBody = bodystr
        )
    else:
        messagebody = {"mediaId": inputs["mediaId"], 
                        "status": status,
                        "errorCause": inputs["error_msg"]}
        bodystr=json.dumps(messagebody)
        print(bodystr)
        response = queue.send_message(
            MessageGroupId = 'media',
            MessageBody = bodystr
        )

if __name__ == "__main__":
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument('--activity_arn', help="Specify an Activity ARN", type=str)
        parser.add_argument('--ip', help="Specify cloudconnector host ip", type=str)
        parser.add_argument('--port', help="Specify port of cloudconnector app", type=str)
        parser.add_argument('--sqs_queue_name', help="Specify sqs_queue_name", type=str)
        parser.add_argument('--logs_bucket_name', help="Specify logs_bucket_name", type=str)
        parser.add_argument('--region', help="Specify region", type=str)
        args=parser.parse_args()
        region = args.region
        print('region '+region)
        
        boto3.setup_default_session(region_name=region)
        sf_client = boto3.client('stepfunctions')
        sqs_resource = boto3.resource('sqs')
        s3_resource = boto3.resource('s3')
        bufferName = args.sqs_queue_name
        logs_bucket_name = args.logs_bucket_name
        queue = sqs_resource.get_queue_by_name(QueueName=bufferName)
        print('Got Client connection with step functions')

        port = '8080'
        if args.port:
            port = args.port

        if args.activity_arn and args.ip:
            activity_arn = args.activity_arn
            ip = args.ip
            rest_api_url = "http://" + args.ip + ":" + port + "/copy"
            print(rest_api_url)
            response = sf_client.get_activity_task(
                activityArn=activity_arn,
                workerName='cloud-connector-worker'
            )
            print("Got Activity Task")
            # inputs to be modified
            input = json.loads(response["input"])
            from_attributes = {}
            from_attributes["kind"] = input["connection"]["kind"]
            from_attributes["container_name"] = input["containerName"]
            from_attributes["item_id"] = input["filePath"] +"/"+ input["fileName"]
            from_attributes["config_map"] = input["connection"]["config_map"]
            to_attributes = {}
            to_attributes["kind"] = "local"
            to_attributes["container_name"] = "efs"
            to_attributes["item_id"] = input['userId'] +"/"+ input['mediaId'] +"/"+ input['fileName']
            to_attributes["config_map"] = {"path": "/mnt"}
            input_param = {}
            input_param["mediaId"] = input["mediaId"]
            input_param["userId"] = input["userId"]
            input_param["fileName"] = input["fileName"]
            input_param["from"] = from_attributes
            input_param["to"] = to_attributes
            input_param["instances"] = input["instances"]
            inputs = input_param
            LOGS_PREFIX = inputs['userId'] + '/' + inputs['mediaId'] +'/copy/'
            token = response['taskToken']
            print(inputs)
            run_worker(token, inputs, rest_api_url, sf_client, queue, s3_resource, logs_bucket_name)

        else:
            print('Please specify Activity ARN and Ip')
            exit(1)
    except Exception as e:
        print(e)
        #s3_resource.object(LOGS_BUCKET_NAME, LOGS_PREFIX+'logs.txt').put(Body = e)
        s3_resource.object(logs_bucket_name, LOGS_PREFIX+'logs.txt').put(Body = e)
        input['error_msg'] = str(e)
        sqs_send_message(queue, inputs, "FAILED")
        exit(1)
