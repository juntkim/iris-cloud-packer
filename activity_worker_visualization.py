import boto3
import sys
import argparse
import requests
import json
import logging
import subprocess
import os
import array
import fnmatch
	
def run_worker(queue, token, inputs, sf_client, logger, names, args):
	try:
		homePath = "/home/ec2-user/"
		logger.info('worker started')
		inputfile = inputs["fileId"].replace(" ", "\ ")
		
		#print("before visaul subprocess"+inputs["fileId"]+" "+inputs["userId"]+" "+inputs["mediaId"])
		media_bucket_name = args.media_bucket_name
		logs_bucket_name = args.logs_bucket_name
		retVal = subprocess.call("~/iris_cloud_visualization "+inputfile+" 7 "+names[0]+" "+homePath,shell=True)
		if retVal == 0:
			send_success_messagebody(queue, inputs, names, 'CREATED')
			subprocess.call("aws s3 cp /home/ec2-user/. s3://"+ media_bucket_name +"/"+inputs["userId"]+"/"+inputs["mediaId"]+"/visualizations --exclude '*' --include '*.json' --recursive", shell=True)
			send_success_messagebody(queue, inputs, names, 'UPLOADED')
			subprocess.call("aws s3 cp /home/ec2-user/. s3://"+ media_bucket_name +"/"+inputs["userId"]+"/"+inputs["mediaId"]+"/visualizations --exclude '*' --include 'log.txt' --recursive", shell=True)
			for file in os.listdir(homePath):
				if fnmatch.fnmatch(file, '*track1*'):
					names2 = []
					names2.append(names[0])
					names2.append(names[0]+'_audiophasemeter0_track1.json')
					names2.append(names[0]+'_audiolevelmeters_track1.json')
					names2.append(names[0]+'_audiowaveform1_track1.json')
					send_success_messagebody(queue, inputs, names2, 'INIT')
					send_success_messagebody(queue, inputs, names2, 'IN_PROGRESS')
					send_success_messagebody(queue, inputs, names2, 'CREATED')
					send_success_messagebody(queue, inputs, names2, 'UPLOADED')
		#	print("after visaul")
			sf_response = sf_client.send_task_success(
				taskToken=token,
				output=json.dumps(inputs))
			logger.info(sf_response)
			logger.info('activity task completed successfully')
			
		else:
			messages = {'201': 'Server Error: notify support (501)', '202': 'Unable to open source video file. (502)', 
				'203': 'Unable to parse source video file (503)', '204': 'Unable to excute the program temporarily. Please try again (504)',
				'205': 'Unable to excute the program. Please try again (505)', '206': 'Source file is not a video file (506)',
				'207': 'Unable to excute the program temporarily. Please try again (507)', '208': 'Source video file format is not supported (508)'}
			errorCause = messages.get(str(retVal))
			send_failure_messagebody(queue, inputs, names, 'FAILED', errorCause)
			subprocess.call("aws s3 cp /home/ec2-user/. s3://"+ logs_bucket_name +"/"+inputs["userId"]+"/"+inputs["mediaId"]+"/visualizations --exclude '*' --include 'log.txt' --recursive", shell=True)
			inputs['error_msg'] = errorCause
			#sf_response = sf_client.send_task_success(
			#	taskToken=token,
			#	output=json.dumps(inputs))
			
			sf_client.send_task_failure(
				taskToken=token,
				error= "Error occurred while running visualization",
				cause=errorCause)

	except Exception as e:
		sf_client.send_task_failure(
			taskToken=token,
			error= "Error occurred while processing visualization",
			cause=e)


def send_success_messagebody(queue, inputs, names, status):
	content = {"visualizations": [{"fileName": names[1], "mediaId": inputs["mediaId"], "status": status, "visualizationTypeId": 3},
		{"fileName": names[2], "mediaId": inputs["mediaId"], "status": status, "visualizationTypeId": 4},
		{"fileName": names[3], "mediaId": inputs["mediaId"], "status": status, "visualizationTypeId": 5}]}
	bodystr=json.dumps(content)
	print(bodystr)
	response = queue.send_message(
		MessageGroupId='visualization',
		MessageBody=bodystr
	)
	
def send_failure_messagebody(queue, inputs, names, status, errorMsg):
	content = {"mediaId": inputs["mediaId"], "status": status, "errorCause": errorMsg}
	bodystr=json.dumps(content)
	print(bodystr)
	response = queue.send_message(
		MessageGroupId='media',
		MessageBody=bodystr
	)
	
		
if __name__ == "__main__":

	try:
		logging.basicConfig()
		logger = logging.getLogger('logger')
		parser = argparse.ArgumentParser()
		parser.add_argument('--activity_arn', help="Specify an Activity ARN", type=str)
		parser.add_argument('--logs_bucket_name', help="Specify logs_bucket_name", type=str)
		parser.add_argument('--media_bucket_name', help="Specify media_bucket_name", type=str)
		parser.add_argument('--sqs_queue_name', help="Specify sqs_queue_name", type=str)
		parser.add_argument('--region', help="Specify region", type=str)
		args=parser.parse_args()
		region = args.region
		boto3.setup_default_session(region_name=region)
		sf_client = boto3.client('stepfunctions')
		sqs_client = boto3.resource('sqs')
	#	bufferName = 'irisCloud.fifo'
		bufferName = args.sqs_queue_name
		queue = sqs_client.get_queue_by_name(QueueName=bufferName)		
		#sqs_url = 'https://sqs.ap-southeast-1.amazonaws.com/852235769060/irisCloudSQS.fifo'
		
		if args.activity_arn:
			activity_arn = args.activity_arn
			print("before get_activity_task")
			response = sf_client.get_activity_task(
				activityArn=activity_arn,
				workerName='visualization-activity-worker'
			)
			print("after get_activity_task")
			logger.info("Got Activity Task")			
			inputs = json.loads(response['input'])
			filename = inputs["fileName"]
			filename = filename.replace(" ", "_")
			names = []
			names.append(filename.rsplit('.', 1)[0])
			names.append(names[0]+'_audiophasemeter0_track0.json')
			names.append(names[0]+'_audiolevelmeters_track0.json')
			names.append(names[0]+'_audiowaveform1_track0.json')
			send_success_messagebody(queue, inputs, names, 'INIT')
			
			print("after load input")
			token = response['taskToken']
			print("before run_worker"+queue.url)
			send_success_messagebody(queue, inputs, names, 'IN_PROGRESS')
			run_worker(queue, token, inputs, sf_client, logger, names, args)

		else:
			logger.info('Please specify Activity ARN and Ip')
			exit(1)
	except Exception as e:
		logger.error("Error occurred while processing the activity task state"+str(e))
		exit(1)

