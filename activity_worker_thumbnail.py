import boto3
import sys
import argparse
import requests
import json
import logging
import subprocess
import os


def run_worker(queue, token, inputs, sf_client, logger, names, args):
	try:
		logger.info('worker started')
		inputfile = inputs["fileId"].replace(" ", "\ ")
		#inputfile = inputs["fileId"]
		s3_bucket_name = args.media_bucket_name
		#print("before visaul thumbnail"+inputs["fileId"]+" "+inputs["userId"]+" "+inputs["mediaId"])
		create_send_messagebody(queue, inputs, names, 'CREATED')
		retVal = subprocess.call("~/thumbnail.sh "+inputfile+ " 20 "+names[1]+" "+inputs["userId"]+" "+inputs["mediaId"]+" /home/ec2-user",shell=True)
		#print("after thumbnail subprocess"+inputs["fileId"])

		if retVal == 0:
			subprocess.call("aws s3 cp /home/ec2-user/. s3://"+s3_bucket_name+"/"+inputs["userId"]+"/"+inputs["mediaId"]+"/thumbnail --exclude '*' --include '*.png' --recursive", shell=True)
			create_send_messagebody(queue, inputs, names, 'UPLOADED')
		
			print("after thumbnail")
			sf_response = sf_client.send_task_success(
				   taskToken=token,
				   output=json.dumps(inputs)
			)
		#	return
		#	logger.info(sf_response)
		#	logger.info('activity task completed successfully')
		else:
			messages = {'401': 'Server Error: notify support (401)'}
		#	names.append(messages.get('401'))
			inputs['error_msg'] = messages.get('401')
		#	names[3] = messages.get('401')
			#print(names)
			create_send_messagebody(queue, inputs, names, 'FAILED')
			sf_client.send_task_failure(
				taskToken=token,
				error="Error occurred while processing thumbnail extraction",
				cause=inputs['error_msg'])


	except Exception as e:
		sf_client.send_task_failure(
			taskToken=token,
			error= "Error occurred while processing visualization",
			cause=e)

def create_send_messagebody(queue, inputs, names, status):
	if status == 'FAILED':
		messagebody = {"mediaId": inputs["mediaId"],
					   "status": status,
					   "errorCause": inputs['error_msg']}
		bodystr = json.dumps(messagebody)
		print(bodystr)
		response = queue.send_message(
			MessageGroupId='media',
			MessageBody=bodystr)
	else:
		content = {"thumbnails": [{"fileName": names[2], "mediaId": inputs["mediaId"], "status": status}]}
		bodystr=json.dumps(content)
		print(bodystr)
		response = queue.send_message(
			MessageGroupId='thumbnail',
			MessageBody=bodystr
		)

if __name__ == "__main__":

	try:
		logging.basicConfig()
		logger = logging.getLogger('logger')
		parser = argparse.ArgumentParser()
		parser.add_argument('--activity_arn', help="Specify an Activity ARN", type=str)
		parser.add_argument('--logs_bucket_name', help="Specify logs_bucket_name", type=str)
		parser.add_argument('--media_bucket_name', help="Specify s3_bucket_name", type=str)
		parser.add_argument('--sqs_queue_name', help="Specify sqs_queue_name", type=str)
		parser.add_argument('--region', help="Specify region", type=str)
		args=parser.parse_args()
		region = args.region
		boto3.setup_default_session(region_name=region)
		sf_client = boto3.client('stepfunctions')
		sqs_client = boto3.resource('sqs')
		#bufferName = 'irisCloud.fifo'
		bufferName = args.sqs_queue_name
		queue = sqs_client.get_queue_by_name(QueueName=bufferName)	

		if args.activity_arn:
			activity_arn = args.activity_arn
			print("before get_activity_task")
			response = sf_client.get_activity_task(
				activityArn=activity_arn,
				workerName='thumbnail-activity'
			)
			print("after get_activity_task")
			logger.info("Got Activity Task")
			inputs = json.loads(response['input'])
			filename = inputs["fileName"]
			filename = filename.replace(" ", "_")
			names = []
			names.append(filename.rsplit('.', 1)[0])
			names.append(names[0]+'_thumbnail')
			names.append(names[0]+'_thumbnail.png')

			print("after load input")
			token = response['taskToken']
			print("before run_worker")
			create_send_messagebody(queue, inputs, names, 'INIT')
			create_send_messagebody(queue, inputs, names, 'IN_PROGRESS')
			run_worker(queue, token, inputs, sf_client, logger, names, args)

		else:
			logger.info('Please specify Activity ARN and Ip')
			
	except Exception as e:
		sf_client.send_task_failure(
			taskToken=token,
			error= "Error occurred while main ",
			cause=e)
		
