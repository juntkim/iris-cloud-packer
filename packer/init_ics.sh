sudo apt-get update -y
sudo apt update -y
sudo apt install openjdk-8-jdk -y
sudo update-java-alternatives -s java-1.8*
sudo wget https://dl.google.com/go/go1.12.9.linux-amd64.tar.gz
sudo tar -C /usr/local -xzf go1.12.9.linux-amd64.tar.gz
export PATH=$PATH:/usr/local/go/bin
sudo cp /usr/local/go/bin/* /usr/bin/
sudo apt install maven -y
