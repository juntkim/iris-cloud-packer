sudo yum update -y
sudo yum install git -y
sudo yum install gcc -y
sudo yum install python3 -y
sudo yum install -y amazon-efs-utils
sudo python3 -m pip install boto3
sudo python3 -m pip install requests
sudo python3 -m pip install logger
sudo python3 -m pip install awscli
git clone https://juntkim@bitbucket.org/juntkim/iris-cloud-packer.git
sudo cp ./iris-cloud-packer/* ~/.
git clone https://juntkim@bitbucket.org/juntkim/iris-cloud-visualization.git
chmod 777 ~/iris-cloud-visualization/sourcecode.sh
cd ~/iris-cloud-visualization
./sourcecode.sh
cd ~
mkdir .aws
echo -e "[default]\naws_access_key_id = $AWS_ACCESS_KEY\naws_secret_access_key = $AWS_SECRET_KEY" > ~/.aws/credentials
echo -e "[default]\nregion = $AWS_REGION" > ~/.aws/config


