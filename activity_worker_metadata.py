import boto3
import sys
import argparse
import requests
import json
import logging
import os
import subprocess

logger = logging.getLogger()
streamHandler = logging.StreamHandler()
streamHandler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)

#LOGS_BUCKET_NAME = 'graymeta-iris-logs'
LOGS_PREFIX = None

def run_worker(token, inputs, sf_client, queue, s3_resource, args):
	try:
		logger.info('worker started')
		print('Worker started')
		inputs['status'] = 'IN_PROGRESS'
		sqs_send_message(queue, inputs, 'SUCCESS')
		logs_bucket_name = args.logs_bucket_name
		media_bucket_name = args.media_bucket_name
		print('Done sqs')
		print('inputs["fileId"] ', inputs["fileId"])
		print('inputs["userId"] ', inputs["userId"])
		print('inputs["mediaId"] ', inputs["mediaId"])
		#command = "~/metadata -file \""+ inputs['fileId'] +"\""+ " -user \"" + inputs['userId'] + "\" -media \"" + inputs['mediaId']+"\""
		#os.system(command)
		retVal = subprocess.call("~/metadata -file \""+ inputs['fileId'] +"\" -user " + inputs['userId'] + " -media " + inputs['mediaId'], shell=True)
		print('Done metadata')
		inputs['status'] = 'CREATED'
		sqs_send_message(queue, inputs, 'SUCCESS')
		print('After execution of metadata command')

		command = "cp /tmp/metadata* ~/metadata.json && aws s3 cp /home/ec2-user/metadata.json s3://"+media_bucket_name+"/\""+ inputs["userId"] +"\"/\""+inputs["mediaId"]+"\"/metadata/"
		os.system(command)
		#retVal = subprocess.call("cp /tmp/metadata* ~/metadata.json && aws s3 cp /home/ec2-user/metadata.json s3://graymeta-iris-media-storage/"+inputs["userId"]+"/"+inputs["mediaId"]+"/metadata/")
		if retVal == 1:
			inputs['status'] = 'UPLOADED'
			sqs_send_message(queue, inputs, 'SUCCESS')
			#logger.info(sf_response)
			logger.info('activity task completed successfully')
			sf_client.send_task_success(
				taskToken=token,
				output=json.dumps(inputs)
			)
			return
		else:
			messages = {'101': 'Server Error: notify support (101)'}
			inputs['error_msg'] = messages.get('101')
			sqs_send_message(queue, inputs, "FAILED")
		#	sf_client.send_task_success(
		#		taskToken=token,
		#		output=json.dumps(inputs)
		#	)
			sf_client.send_task_failure(
				taskToken=token,
				error="Error occurred while processing metadata extraction",
				cause=inputs['error_msg'])
			print("Done sf_client.send_task_failure")
			s3_resource.object(logs_bucket_name, inputs["userId"]+"/"+inputs["mediaId"]+"/" + 'logs.txt').put(Body=str(inputs['error_msg']))
		#	s3_resource.object(LOGS_BUCKET_NAME, inputs["userId"]+"/"+inputs["mediaId"]+"/" + 'logs.txt').put(Body=str(inputs['error_msg']))
			print("Done s3_resource.object.put")
			return
	except Exception as e:
		sf_client.send_task_failure(
			taskToken=token,
			error= "Error occurred while processing metadata extraction",
			cause=e)
		inputs['error_msg'] = e
		sqs_send_message(queue, inputs, "FAILED")
		s3_resource.object(logs_bucket_name, LOGS_PREFIX+'logs.txt').put(Body = e)
	#	s3_resource.object(LOGS_BUCKET_NAME, LOGS_PREFIX+'logs.txt').put(Body = e)

def sqs_send_message(queue, inputs, status):

	if status =='SUCCESS':
		messagebody = {"mediaId": inputs["mediaId"], "fileName": "metadata.json", "status": inputs['status']}
		bodystr=json.dumps(messagebody)
		print(bodystr)
		response = queue.send_message(
			MessageGroupId = 'metadata',
			MessageBody = bodystr)
	else:
		messagebody = {"mediaId": inputs["mediaId"],
					   "status": status,
					   "errorCause": inputs["error_msg"]}
		bodystr=json.dumps(messagebody)
		print(bodystr)
		response = queue.send_message(
			MessageGroupId = 'media',
			MessageBody = bodystr)

if __name__ == "__main__":
	try:
		parser = argparse.ArgumentParser()
		parser.add_argument('--activity_arn', help="Specify an Activity ARN", type=str)
		parser.add_argument('--logs_bucket_name', help="Specify logs_bucket_name", type=str)
		parser.add_argument('--media_bucket_name', help="Specify media_bucket_name", type=str)
		parser.add_argument('--sqs_queue_name', help="Specify sqs_queue_name", type=str)
		parser.add_argument('--region', help="Specify region", type=str)
		args=parser.parse_args()
		if args.activity_arn == None:
			print('Usage: python activity_worker_metadata.py --activity_arn value')
			sys.exit(1)
		print('Step functions started')
		region = args.region
		boto3.setup_default_session(region_name=region)
		sf_client = boto3.client('stepfunctions')
		sqs_resource = boto3.resource('sqs')
		s3_resource = boto3.resource('s3')
		#bufferName = 'irisCloud.fifo'
		bufferName=args.sqs_queue_name
		queue = sqs_resource.get_queue_by_name(QueueName=bufferName)
		media_bucket_name = args.media_bucket_name
		
		print("Activity Arn:", args.activity_arn)
		if args.activity_arn is not None:
			activity_arn = args.activity_arn
			response = sf_client.get_activity_task(
				activityArn=activity_arn,
				workerName='metadata-extractor-worker')
			logger.info("Got Activity Task")
			print('Got Activity Task')
			inputs = json.loads(response['input'])
			LOGS_PREFIX = inputs['userId'] + '/' + inputs['mediaId'] +'/metadata/'
			inputs['status'] = 'INIT'
			sqs_send_message(queue, inputs, 'SUCCESS')
			token = response['taskToken']
			run_worker(token, inputs, sf_client, queue, s3_resource, args)
		else:
			logger.info('Please specify Activity ARN and Ip')
			exit(1)
	except Exception as e:
		logger.error("Error occurred while processing the activity task state")
		print("Error :", e)
		inputs['error_msg'] = e
		sqs_send_message(queue, inputs, "FAILED")
		s3_resource.object(media_bucket_name, LOGS_PREFIX+'logs.txt').put(Body = e)
	#	s3_resource.object(LOGS_BUCKET_NAME, LOGS_PREFIX+'logs.txt').put(Body = e)
		exit(1)
